// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#include <dolfin.h>
#include <FenicsSolidMechanics.h>
#include "../forms/p2_forms/Plas3D.h"

using namespace dolfin;

// Right-hand side (body force)
class Source : public Expression
{
public:

  Source(const double& t) : Expression(3), t(t) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    // Body force
    values[0] =  0.0;
    values[1] =  0.0;
    values[2] = -40.0*sin((t/2.0)*DOLFIN_PI);
  }
  const double& t;
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryXY : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    // Locate the line (0, 0, 0.5) - (0, 1, 0.5)
    return (x[0] < DOLFIN_EPS && std::abs(x[2] - 0.5) < DOLFIN_EPS);
  }
};


// Sub domain for Dirichlet boundary condition
class DirichletBoundaryZ : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    // Locate the lines (0, 0, 0.5) - (0, 1, 0.5) and (5, 0, 0.5) - (5, 1, 0.5)
    return (x[0] < DOLFIN_EPS && std::abs(x[2] - 0.5) < DOLFIN_EPS)
                   || (std::abs(x[0] - 5.0) < DOLFIN_EPS
                       && std::abs(x[2] - 0.5) < DOLFIN_EPS);
  }
};

int main()
{
  Timer timer("Total plasicity solver time");

  auto mesh = std::make_shared<Mesh>("beam10000.xml.gz");

  // Young's modulus and Poisson's ratio
  double E = 20000.0;
  double nu = 0.3;

  // Final time and time step
  const double T = 1.0;
  double dt = 0.1;
  double t = 0.0;

  // Source term, RHS
  auto f = std::make_shared<Source>(t);

  // Function spaces
  auto V = std::make_shared<Plas3D::CoefficientSpace_f>(mesh);
  dolfin::cout << "Number of dofs: " << V->dim() << dolfin::endl;

  // Extract elements for stress and tangent
  std::shared_ptr<const FiniteElement> element_t;
  {
    Plas3D::Form_a::CoefficientSpace_t Vt(mesh);
    element_t = Vt.element();
  }

  auto Vs = std::make_shared<Plas3D::Form_L::CoefficientSpace_s>(mesh);
  auto element_s = Vs->element();

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  auto V0 = V->sub(0);
  auto V1 = V->sub(1);
  auto V2 = V->sub(2);

  auto zero = std::make_shared<Constant>(0.0);
  auto dbXY = std::make_shared<DirichletBoundaryXY>();
  auto dbZ = std::make_shared<DirichletBoundaryZ>();
  auto bc0 = std::make_shared<DirichletBC>(V0, zero, dbXY, "pointwise");
  auto bc1 = std::make_shared<DirichletBC>(V1, zero, dbXY, "pointwise");
  auto bc2 = std::make_shared<DirichletBC>(V2, zero, dbZ, "pointwise");

  std::vector<std::shared_ptr<const DirichletBC>> bcs = {bc0, bc1, bc2};

  // Slope of hardening (linear) and hardening parameter
  const double E_t = 0.3*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 200.0;

  // Solution function
  auto u = std::make_shared<Function>(V);

  // Object of class von Mises
  const auto J2 = std::make_shared<fenicssolid::VonMises>(E, nu, yield_stress,
                                                          hardening_parameter);
  auto eps_form = std::make_shared<Plas3D::Form_eps_form>(Vs);
  eps_form->f = u;
  auto mesh_ref = std::make_shared<Mesh>(UnitTetrahedronMesh::create());
  auto Vs_ref = std::make_shared<Plas3D::CoefficientSpace_s>(mesh_ref);
  auto eps_ref_form = std::make_shared<Plas3D::Form_eps_ref_form>(Vs_ref);

  // Strain UFLQuadratureFunction
  const auto Qdef = std::make_shared<fenicssolid::UFLQuadratureFunction>(eps_form, eps_ref_form);

  // Constituive update
  auto constitutive_update
    = std::make_shared<fenicssolid::ConstitutiveUpdate>(Qdef, J2);

  // Create forms and attach functions
  auto tangent
    = std::make_shared<fenicssolid::QuadratureFunction>(mesh, element_t,
                                                        constitutive_update,
                                                        constitutive_update->w_tangent());

  auto a = std::make_shared<Plas3D::Form_a>(V, V);
  a->t = tangent;

  auto L = std::make_shared<Plas3D::Form_L>(V);
  L->f = f;
  
  auto stress = std::make_shared<fenicssolid::QuadratureFunction>(mesh, element_s,
                                                                  constitutive_update->w_stress());
  L->s = stress;

  // Create PlasticityProblem
  auto nonlinear_problem
    = std::make_shared<fenicssolid::PlasticityProblem>(a, L, u, tangent,
                                                       stress, bcs);

  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "incremental";
  nonlinear_solver.parameters["maximum_iterations"]    = 50;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

  // File names for output
  File file1("output/disp.pvd");
  File file2("output/eq_plas_strain.pvd");

  // Equivalent plastic strain for visualisation
  auto eps_eq = std::make_shared<MeshFunction<double>>(mesh, mesh->topology().dim());

  // Load-disp info
  unsigned int step = 0;
  while (t < T)
  {
    t += dt;
    step++;
    std::cout << "step begin: " << step << std::endl;
    std::cout << "time: " << t << std::endl;

    // Solve non-linear problem
    nonlinear_solver.solve(*nonlinear_problem, *u->vector());

    // Update variables
    constitutive_update->update_history();

    // Write output to files
    file1 << *u;
    constitutive_update->eps_p_eq()->compute_mean(eps_eq);
    file2 << *eps_eq;
  }

  cout << "Solution norm: " << u->vector()->norm("l2") << endl;

  timer.stop();
  dolfin::list_timings(dolfin::TimingClear::clear, {dolfin::TimingType::wall});

  return 0;
}
