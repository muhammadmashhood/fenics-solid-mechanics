# FEniCS Solid Mechanics

> A major re-write and update of this library is starting.
> The new version will support a wider class of problems, and 
> will include a Python interface.

The aim of this library is to provide functionality that makes it
simple for application developers to create solvers for complicated
problems.  Although focus is on solid mechanics problems the provided
abstractions are developed with the aim of also being useful in other
areas.

Another goal of this library is to show how FEniCS software can be
combined in more complex applications and thereby serve as an
additional reference in extension to the simpler demos found in FFC
and DOLFIN.  For instance, this library implements the Von Mises and
Drucker-Prager plasticity models using FEniCS software. However, the
structure of the library should be general enough to also allow other
models to be implemented with little effort. For 2D cases plane strain
is assumed.


## Dependencies

The development version of this library aims to track the development
versions of the FEniCS components. Releases will be made at the same
time as releases of FEniCS, starting with FEniCS/DOLFIN 1.7.


## Installation

FEniCS Solid Mechanics depends on the main FEniCS libraries.  FEniCS
Solid Mechanics uses CMake for build an configuration, and to build
the library CMake must be able to find the DOLFIN configuration
files. To build in a directory inside the source tree, run:

    > ./cmake.local


## Compiling against the library

To compile against the plasticity library you first need to update the
system variables LD_LIBRARY_PATH and PKG_CONFIG_PATH. This is most
conveniently done by sourcing the file fenics-solid-mechanics.conf:

    source fenics-solid-mechanics.conf


## Feedback

Feed back and comments should be sent to

* Kristian B. Ølgaard (<k.b.oelgaard@gmail.com>)
* Garth N. Wells (<gnw20@cam.ac.uk>)

Bugs can be registerd at
<https://bitbucket.org/fenics-apps/fenics-solid-mechanics/issues> and
pull requests made at
<https://bitbucket.org/fenics-apps/fenics-solid-mechanics/pull-requests>.


## License

This library is licensed under the GNU LGPL Version 3.0, see
`COPYING.LESSER`.
