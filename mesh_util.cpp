#include <array>
#include <iostream>
#include <memory>
#include <string>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include <dolfin/geometry/Point.h>
#include <dolfin/mesh/CellType.h>
#include <dolfin/generation/UnitTetrahedronMesh.h>


namespace py = pybind11;

#using namespace dolfin;

PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace dolfin_wrapper
{
    void mesh_util(py::module& m)
    {
        py::class_<dolfin::UnitTetrahedronMesh>(m, "UnitTetrahedronMesh")
            .def_static("create", &dolfin::UnitTetrahedronMesh::create);
    }
}