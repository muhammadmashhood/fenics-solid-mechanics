from dolfin import *
import fsm

#set_log_level(10)

Time = 0.

Source = Constant((0.0, 0., -40.0*sin((Time/2.0)*DOLFIN_PI)))

class DirichletBoundaryXY(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS and abs(x[2] - 0.5) < DOLFIN_EPS

class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS and abs(x[2] - 0.5) < DOLFIN_EPS) or \
               (abs(x[0] - 5.0) < DOLFIN_EPS and abs(x[2] - 0.5) < DOLFIN_EPS)

mesh = Mesh("beam10000.xml.gz");

E = 20000.0;
nu = 0.3;

FinalTime = 1.0;
nTimeSteps = 10
TimeIncrement = FinalTime / nTimeSteps;

scheme = "default"
degree = 3
dx = Measure("dx")
dx = dx(degree=degree, scheme=scheme)

V  = VectorFunctionSpace(mesh, "Lagrange", 2)
element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
Vt = FunctionSpace(mesh, element_t)
element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Vs = FunctionSpace(mesh, element_s)

zero = Constant(0.0)


bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryXY(), method="pointwise")
bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryXY(), method="pointwise")
bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryZ(),  method="pointwise")

bcs = [bc0, bc1, bc2]

E_t = 0.3*E
hardening_parameter = E_t/(1.0 - E_t/E)
yield_stress = 200.0

u = Function(V)

def eps(u):
    defgrad = grad(u) + Identity(3)
    epsilon = 0.5 * (dot(defgrad.T, defgrad) - Identity(3))
    return as_vector([epsilon[i, i] for i in range(3)] + [2 * epsilon[i, j] for i, j in [(0, 1), (0, 2), (1, 2)]])

def sigma(s):
    #s = ss.function_space()
    return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

def tangent(t):
    #t = tt.function_space()
    return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
Qdef = fsm.UFLQuadratureFunction(eps(u), element_s, mesh)
fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
#fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
#fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())


v = TestFunction(V)
uTrial = TrialFunction(V)

delta_eps = derivative(eps(u), u, v)
de_eps = derivative(eps(u), u, uTrial)
de_delta_eps = derivative(delta_eps, u, uTrial)

a = inner(delta_eps, dot(tangent(fsm_tangent), de_eps) )*dx + \
    inner(de_delta_eps, fsm_stress)*dx
L = inner(delta_eps, fsm_stress)*dx - inner(v, Source)*dx

nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs)

nonlinear_solver = NewtonSolver()
nonlinear_solver.parameters["convergence_criterion"] = "incremental";
nonlinear_solver.parameters["maximum_iterations"]    = 50;
nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

# File names for output
file1 = File("output/disp.pvd");
#file2 = File("output/eq_plas_strain.pvd");

eps_eq = MeshFunction("double", mesh, mesh.topology().dim());
#fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

step = 0
while (step < nTimeSteps):
    Time += TimeIncrement;
    step = step + 1;
    print("step begin: ", step)
    print("time: ", Time)

    Source.assign(Constant((0.0, 0., -40.0*sin((Time/2.0)*DOLFIN_PI))))
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());

    # Update variables
    fsm_constitutive_update.update_history();

    # Write output to files
    file1 << u;
    #constitutive_update.eps_p_eq().compute_mean(eps_eq);
    #file2 << eps_eq;
