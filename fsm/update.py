import ufl
import dolfin



from fsm.python import cpp
status_update = cpp.status_update
plasticity_model = cpp.plasticity_model


from fsm.ufl_quadrature import UFLQuadratureFunction
from fsm.history import HistoryData

#       .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
#            std::shared_ptr<const dolfin::FiniteElement>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")
#       .def(py::init<std::shared_ptr<const dolfin::Mesh>,
#            std::shared_ptr<const dolfin::FiniteElement>,
#            std::shared_ptr<fsm::StateUpdate>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")

#fsm_constitutive_update = ConstitutiveUpdate(u, Vs, J2)

class ConstitutiveUpdateBaseClass():
    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        self.AlreadyLinked = False

def BuildHistoryWrapper(cpp_history, quad_function):
    quad_degree = quad_function.Element.degree()
    quad_scheme = quad_function.Element._quad_scheme
    mesh = quad_function.Mesh
    cell = quad_function.Mesh.ufl_cell()
    rank = cpp_history.value_rank()
    if rank==0:
        element = dolfin.FiniteElement("Quadrature", cell, degree=quad_degree, quad_scheme=quad_scheme)
    else:
        dim = cpp_history.value_shape[0]
        element = dolfin.VectorElement("Quadrature", cell, degree=quad_degree, dim=dim, quad_scheme=quad_scheme)
    space = dolfin.FunctionSpace(mesh, element)
    return HistoryData(cpp_history, space)

class ConstitutiveUpdate(ConstitutiveUpdateBaseClass):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        ConstitutiveUpdateBaseClass.__init__(self)

        if len(args) != 2:
            raise RuntimeError("Expecting 2 arguments, got ", len(args), ".")

        if not isinstance(args[0], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 1, got ", type(args[0]), ".")
        self.Function = args[0]
        if not isinstance(args[1], list):
            raise RuntimeError("Expecting a fsm 'list' as argument 2, got ", type(args[1]), ".")
        self.Plasticity_Model_per_cell = args[1]
        self._cpp_object = status_update.ConstitutiveUpdate(self.Function.cpp_object(), self.Plasticity_Model_per_cell)

    def name(self):
        return self._cpp_object.name()
    def w_stress(self):
        return self._cpp_object.w_stress()
    def w_tangent(self):
        return self._cpp_object.w_tangent()
    def cpp_object(self):
        return self._cpp_object
    def update(self):
        return self._cpp_object.update()
    def update_history(self):
        return self._cpp_object.update_history()
    def eps_p_eq(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p_eq(), self.Function)
    def eps_p(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p(), self.Function)

class AnbaConstitutiveUpdate(ConstitutiveUpdateBaseClass):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        ConstitutiveUpdateBaseClass.__init__(self)

        if len(args) != 4:
            raise RuntimeError("Expecting 4 arguments, got ", len(args), ".")

        if not isinstance(args[0], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 1, got ", type(args[0]), ".")
        self.Function = args[0]
        if not isinstance(args[1], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 2, got ", type(args[1]), ".")
        self.Function_z = args[1]
        if not isinstance(args[2], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 3, got ", type(args[2]), ".")
        self.Function_zz = args[2]
        if not isinstance(args[3], plasticity_model.PlasticityModel):
            raise RuntimeError("Expecting a 'PlasticityModel' as argument 4, got ", type(args[3]), ".")
        self.PlasticityModel = args[3]

        self._cpp_object = status_update.AnbaConstitutiveUpdate(self.Function.cpp_object(), 
            self.Function_z.cpp_object(), 
            self.Function_zz.cpp_object(), 
	    self.PlasticityModel)

    def name(self):
        return self._cpp_object.name()
    def w_stress(self):
        return self._cpp_object.w_stress()
    def w_stress_z(self):
        return self._cpp_object.w_stress_z()
    def w_stress_zz(self):
        return self._cpp_object.w_stress_zz()
    def w_tangent(self):
        return self._cpp_object.w_tangent()
    def w_tangent_sz_ez(self):
        return self._cpp_object.w_tangent_sz_ez()
    def w_tangent_szz_ez(self):
        return self._cpp_object.w_tangent_szz_ez()
    def w_tangent_szz_ezz(self):
        return self._cpp_object.w_tangent_szz_ezz()
    def cpp_object(self):
        return self._cpp_object
    def update(self):
        return self._cpp_object.update()
    def update_history(self):
        return self._cpp_object.update_history()
    def eps_p_eq(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p_eq(), self.Function)
    def eps_p(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p(), self.Function)

class AnbaConstitutiveUpdateCubic(ConstitutiveUpdateBaseClass):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        ConstitutiveUpdateBaseClass.__init__(self)

        if len(args) != 6:
            raise RuntimeError("Expecting 6 arguments, got ", len(args), ".")

        if not isinstance(args[0], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 1, got ", type(args[0]), ".")
        self.Function = args[0]
        if not isinstance(args[1], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 2, got ", type(args[1]), ".")
        self.Function_z = args[1]
        if not isinstance(args[2], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 3, got ", type(args[2]), ".")
        self.Function_zz = args[2]
        if not isinstance(args[3], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 4, got ", type(args[2]), ".")
        self.Function_zzz = args[3]
        if not isinstance(args[4], UFLQuadratureFunction):
            raise RuntimeError("Expecting a fsm 'UFLQuadratureFunction' as argument 5, got ", type(args[2]), ".")
        self.Function_zzzz = args[4]
        if not isinstance(args[5], plasticity_model.PlasticityModel):
            raise RuntimeError("Expecting a 'PlasticityModel' as argument 6, got ", type(args[3]), ".")
        self.PlasticityModel = args[5]

        self._cpp_object = status_update.AnbaConstitutiveUpdateCubic(self.Function.cpp_object(), 
            self.Function_z.cpp_object(), 
            self.Function_zz.cpp_object(), 
            self.Function_zzz.cpp_object(), 
            self.Function_zzzz.cpp_object(), 
	        self.PlasticityModel)

    def name(self):
        return self._cpp_object.name()
    def w_stress(self):
        return self._cpp_object.w_stress()
    def w_stress_z(self):
        return self._cpp_object.w_stress_z()
    def w_stress_zz(self):
        return self._cpp_object.w_stress_zz()
    def w_stress_zzz(self):
        return self._cpp_object.w_stress_zzz()
    def w_stress_zzzz(self):
        return self._cpp_object.w_stress_zzzz()
    def w_tangent(self):
        return self._cpp_object.w_tangent()
    def w_tangent_sz_ez(self):
        return self._cpp_object.w_tangent_sz_ez()
    def w_tangent_szz_ez(self):
        return self._cpp_object.w_tangent_szz_ez()
    def w_tangent_szz_ezz(self):
        return self._cpp_object.w_tangent_szz_ezz()
    def cpp_object(self):
        return self._cpp_object
    def update(self):
        return self._cpp_object.update()
    def update_history(self):
        return self._cpp_object.update_history()
    def eps_p_eq(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p_eq(), self.Function)
    def eps_p(self):
        return BuildHistoryWrapper(self._cpp_object.eps_p(), self.Function)
