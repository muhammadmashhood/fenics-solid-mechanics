
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/log/log.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <fsm/src/PlasticityModel.h>
#include <fsm/src/VonMises.h>
#include <fsm/src/DruckerPrager.h>

namespace py = pybind11;


//PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fsm_wrappers
{
  void plasticity_model(py::module& m)
  {
    py::class_<fsm::PlasticityModel, std::shared_ptr<fsm::PlasticityModel>>
      (m, "PlasticityModel", "Base class PlasticityModel")
      .def("hardening_parameter", &fsm::PlasticityModel::hardening_parameter)
      .def("kappa", &fsm::PlasticityModel::kappa)
      .def("f", &fsm::PlasticityModel::f)
      .def("df", &fsm::PlasticityModel::df)
      .def("dg", &fsm::PlasticityModel::dg)
      .def("ddg", &fsm::PlasticityModel::ddg)
    ;

    py::class_<fsm::DruckerPrager, std::shared_ptr<fsm::DruckerPrager>, fsm::PlasticityModel>
      (m, "DruckerPrager", "Class DruckerPrager")
      .def(py::init<double, double, double, double, double, double>(), "Create a DruckerPrager PlasticityModel")
      .def("hardening_parameter", &fsm::DruckerPrager::hardening_parameter)
      .def("kappa", &fsm::DruckerPrager::kappa)
      .def("f", &fsm::DruckerPrager::f)
      .def("df", &fsm::DruckerPrager::df)
      .def("dg", &fsm::DruckerPrager::dg)
      .def("ddg", &fsm::DruckerPrager::ddg)
    ;

    py::class_<fsm::VonMises, std::shared_ptr<fsm::VonMises>, fsm::PlasticityModel>
      (m, "VonMises", "Class VonMises")
      .def(py::init<double, double, double, double>(), "Create a VonMises PlasticityModel")
      .def("hardening_parameter", &fsm::VonMises::hardening_parameter)
      .def("kappa", &fsm::VonMises::kappa)
      .def("f", &fsm::VonMises::f)
      .def("df", &fsm::VonMises::df)
      .def("dg", &fsm::VonMises::dg)
      .def("ddg", &fsm::VonMises::ddg)
    ;
  }
}
