import os
from fsm.python.compile_package import compile_package
cpp = compile_package(
    "fsm",
    os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../..")),
    "src/HistoryData.cpp",
    "src/QuadratureFunction.cpp",
    "src/DruckerPrager.cpp",
    "src/VonMises.cpp",
    "src/PlasticityModel.cpp",
    "src/AnbaConstitutiveUpdate.cpp",
    "src/AnbaConstitutiveUpdateCubic.cpp",
    "src/ConstitutiveUpdate.cpp",
    "src/ReturnMapping.cpp",
    "src/PlasticityProblem.cpp",
	"src/UFLQuadratureFunction.cpp"
)

__all__ = [
    'cpp'
]

