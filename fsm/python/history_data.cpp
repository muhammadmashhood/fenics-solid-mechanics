
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/log/log.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <fsm/src/HistoryData.h>

namespace py = pybind11;


//PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fsm_wrappers
{
  void history_data(py::module& m)
  {
    py::class_<fsm::HistoryData, std::shared_ptr<fsm::HistoryData>, dolfin::GenericFunction>
      (m, "HistoryData", "Class to ease the handling of history-dependent values", py::multiple_inheritance())
      .def(py::init<std::shared_ptr<const dolfin::Mesh>,
           std::shared_ptr<const dolfin::FiniteElement>,
           const std::size_t>(), "Create an HistoryData instance")
//       FIXME MISSING WRAPPERS 
//       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 3, 3>>) //6,6
//       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 3, 3>>) //6,6
//       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 6, 1>>)
//       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 6, 1>>)
//       .def("get_old_values", &fsm::HistoryData::get_old_values<Eigen::Matrix<double, 1, 1>>)
//       .def("set_new_values", &fsm::HistoryData::set_new_values<Eigen::Matrix<double, 1, 1>>)
      .def("update_history", &fsm::HistoryData::update_history)
      .def("compute_mean", &fsm::HistoryData::compute_mean)
      .def("old_data", &fsm::HistoryData::old_data)
      .def("hash_old", &fsm::HistoryData::hash_old)
      .def("hash_current", &fsm::HistoryData::hash_current)
      .def("function_space", &fsm::HistoryData::function_space)
      .def("value_rank", &fsm::HistoryData::value_rank)
      .def("value_dimension", &fsm::HistoryData::value_dimension)
      .def_property_readonly("value_shape", &fsm::HistoryData::value_shape)
      .def("restrict", &fsm::HistoryData::restrict)
      .def("compute_vertex_values", &fsm::HistoryData::compute_vertex_values)
//       .def("element", &fsm::HistoryData::element)
    ;
  }
}
