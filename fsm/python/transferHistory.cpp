#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <cmath>
#include <vector>
#include <memory>
#include <Eigen/Core>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>
#include <QuadratureFunction.h>
#include <dolfin/mesh/MeshFunction.h>
#include <HistoryData.h>
namespace anba {
class TransferHistoryData: public HistorydData 
{
  private: 
	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>>  sub_material_id_old;
	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>>  sub_material_id_new;
	const std::shared_ptr<const HistoryData>> historydata_old;
	const std::shared_ptr<const HistoryData>> historydata_new;
	const std::shared_ptr<const dolfin::Mesh>> fullmesh;
  public:
 	TransferHistoryData & operator=(TransferHistoryData&) = delate;
	TransferHistoryData(const TransferHistoryData&)=delete;
	//constructor
	TransferHistoryData(const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> _materialID_old,
			    const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> _materialID_new,
			    const std::shared_ptr<const dolfin::Mesh> _meshComplete,
			    const std::shared_ptr<const HistoryData>> _historydata_old,
			    const std::shared_ptr<const HistoryData>> _historydata_new)
				sub_material_id_old(_materialID_old),
				sub_material_id_new(_materialID_new),
				fullmesh(_meshComplete),
				historydata_old(_historydata_old),
				historydata_new(_historydata_new)
	std::shared_ptr<const HistoryData>> const 
	{
	  return historydata_new;
	}
	// make 2 methods.
}; // class
} // namespace
PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
PYBIND11_MODULE(SIGNATURE, m)
{
    pybind11::class_<anba::TransferHistoryData, std::shared_ptr<anba::TransferHistoryData>, anba::HistoryData > 
      (m, "TransferHistoryData ", "Class to transfer different HistoryData", pybind11::multiple_inheritance())
      .def(pybind11::init<
const std::shared_ptr<const dolfin::MeshFunction<std::size_t>>,
			    const std::shared_ptr<const dolfin::MeshFunction<std::size_t>>,
			    const std::shared_ptr<const dolfin::Mesh> ,
			    const std::shared_ptr<const HistoryData>> ,
			    const std::shared_ptr<const HistoryData>>> (),
         "Create a TransferHistoryData")
} // close binding
