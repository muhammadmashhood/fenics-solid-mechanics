#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <fsm/src/UFLQuadratureFunction.h>

namespace py = pybind11;

//PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fsm_wrappers
{
  void ufl_quadrature_function(py::module& m)
  {
    py::class_<fsm::UFLQuadratureFunction, std::shared_ptr<fsm::UFLQuadratureFunction>, dolfin::GenericFunction>
      (m, "UFLQuadratureFunction", "Class to compute UFL expression at integration point values", py::multiple_inheritance())
      .def(py::init<std::shared_ptr<const dolfin::Form>, 
                    std::shared_ptr<const dolfin::Form>>(), "Create a UFLQuadratureFunction")
      .def("function_space", &fsm::UFLQuadratureFunction::function_space)
      .def("value_rank", &fsm::UFLQuadratureFunction::value_rank)
      .def("value_dimension", &fsm::UFLQuadratureFunction::value_dimension)
      .def_property_readonly("value_shape", &fsm::UFLQuadratureFunction::value_shape)
      .def("restrict", &fsm::UFLQuadratureFunction::restrict)
      .def("compute_vertex_values", &fsm::UFLQuadratureFunction::compute_vertex_values)
      .def("element", &fsm::UFLQuadratureFunction::element)
//      .def("id", &fsm::QuadratureFunction::id);
    ;
  }
}
