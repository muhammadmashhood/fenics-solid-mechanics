
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>
#include <pybind11/stl_bind.h>

#include <dolfin/log/log.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/fem/GenericDofMap.h>

#include <fsm/src/StateUpdate.h>
#include <fsm/src/UFLQuadratureFunction.h>
#include <fsm/src/ConstitutiveUpdate.h>
#include <fsm/src/AnbaConstitutiveUpdate.h>
#include <fsm/src/AnbaConstitutiveUpdateCubic.h>
#include <fsm/src/PlasticityModel.h>

namespace py = pybind11;


//PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fsm_wrappers
{
  void status_update(py::module& m)
  {
    py::class_<std::vector<double>>(m, "DoubleVector", "fsm DoubleVector")
        .def(py::init<>() )
        .def("clear", &std::vector<double>::clear)
        .def("pop_back", &std::vector<double>::pop_back)
        .def("__len__", [](const std::vector<double> &v) { return v.size(); })
        .def("__iter__", [](std::vector<double> &v) {
            return py::make_iterator(v.begin(), v.end());
        }, py::keep_alive<0, 1>())
    ; /* Keep vector alive while iterator is used */    
    py::class_<fsm::StateUpdate, std::shared_ptr<fsm::StateUpdate>>
      (m, "StateUpdate", "Base class StateUpdate")
      .def("update", &fsm::StateUpdate::update)
    ;
    py::class_<fsm::ConstitutiveUpdate, std::shared_ptr<fsm::ConstitutiveUpdate>, fsm::StateUpdate>
      (m, "ConstitutiveUpdate", "Class ConstitutiveUpdate")
      .def(py::init<std::shared_ptr<const fsm::UFLQuadratureFunction>,
           const std::vector<std::shared_ptr<const fsm::PlasticityModel>>>(), "Create a ConstitutiveUpdate instance")
//       .def("update", &fsm::ConstitutiveUpdate::update)
      .def("update_history", &fsm::ConstitutiveUpdate::update_history)
      .def("w_stress", &fsm::ConstitutiveUpdate::w_stress, py::return_value_policy::reference_internal)
      .def("w_tangent", &fsm::ConstitutiveUpdate::w_tangent, py::return_value_policy::reference_internal)
      .def("eps_p_eq", &fsm::ConstitutiveUpdate::eps_p_eq)
      .def("eps_p", &fsm::ConstitutiveUpdate::eps_p)
    ;
    py::class_<fsm::AnbaConstitutiveUpdate, std::shared_ptr<fsm::AnbaConstitutiveUpdate>, fsm::StateUpdate>
      (m, "AnbaConstitutiveUpdate", "Class AnbaConstitutiveUpdate")
      .def(py::init<std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::PlasticityModel>>(), "Create an AnbaConstitutiveUpdate instance")
//       .def("update", &fsm::ConstitutiveUpdate::update)
      .def("update_history", &fsm::AnbaConstitutiveUpdate::update_history)
      .def("w_stress", &fsm::AnbaConstitutiveUpdate::w_stress, py::return_value_policy::reference_internal)
      .def("w_stress_z", &fsm::AnbaConstitutiveUpdate::w_stress_z, py::return_value_policy::reference_internal)
      .def("w_stress_zz", &fsm::AnbaConstitutiveUpdate::w_stress_zz, py::return_value_policy::reference_internal)
      .def("w_tangent", &fsm::AnbaConstitutiveUpdate::w_tangent, py::return_value_policy::reference_internal)
      .def("w_tangent_sz_ez", &fsm::AnbaConstitutiveUpdate::w_tangent_sz_ez, py::return_value_policy::reference_internal)
      .def("w_tangent_szz_ez", &fsm::AnbaConstitutiveUpdate::w_tangent_szz_ez, py::return_value_policy::reference_internal)
      .def("w_tangent_szz_ezz", &fsm::AnbaConstitutiveUpdate::w_tangent_szz_ezz, py::return_value_policy::reference_internal)
      .def("eps_p_eq", &fsm::AnbaConstitutiveUpdate::eps_p_eq)
      .def("eps_p", &fsm::AnbaConstitutiveUpdate::eps_p)
    ;
    py::class_<fsm::AnbaConstitutiveUpdateCubic, std::shared_ptr<fsm::AnbaConstitutiveUpdateCubic>, fsm::StateUpdate>
      (m, "AnbaConstitutiveUpdateCubic", "Class AnbaConstitutiveUpdateCubic")
      .def(py::init<std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::UFLQuadratureFunction>,
           std::shared_ptr<const fsm::PlasticityModel>>(), "Create an AnbaConstitutiveUpdateCubic instance")
//       .def("update", &fsm::ConstitutiveUpdate::update)
      .def("update_history", &fsm::AnbaConstitutiveUpdateCubic::update_history)
      .def("w_stress", &fsm::AnbaConstitutiveUpdateCubic::w_stress, py::return_value_policy::reference_internal)
      .def("w_stress_z", &fsm::AnbaConstitutiveUpdateCubic::w_stress_z, py::return_value_policy::reference_internal)
      .def("w_stress_zz", &fsm::AnbaConstitutiveUpdateCubic::w_stress_zz, py::return_value_policy::reference_internal)
      .def("w_stress_zzz", &fsm::AnbaConstitutiveUpdateCubic::w_stress_zzz, py::return_value_policy::reference_internal)
      .def("w_stress_zzzz", &fsm::AnbaConstitutiveUpdateCubic::w_stress_zzzz, py::return_value_policy::reference_internal)
      .def("w_tangent", &fsm::AnbaConstitutiveUpdateCubic::w_tangent, py::return_value_policy::reference_internal)
      .def("w_tangent_sz_ez", &fsm::AnbaConstitutiveUpdateCubic::w_tangent_sz_ez, py::return_value_policy::reference_internal)
      .def("w_tangent_szz_ez", &fsm::AnbaConstitutiveUpdateCubic::w_tangent_szz_ez, py::return_value_policy::reference_internal)
      .def("w_tangent_szz_ezz", &fsm::AnbaConstitutiveUpdateCubic::w_tangent_szz_ezz, py::return_value_policy::reference_internal)
      .def("eps_p_eq", &fsm::AnbaConstitutiveUpdateCubic::eps_p_eq)
      .def("eps_p", &fsm::AnbaConstitutiveUpdateCubic::eps_p)
    ;
  }
}
