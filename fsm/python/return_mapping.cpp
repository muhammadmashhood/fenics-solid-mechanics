
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/la/GenericVector.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/fem/SystemAssembler.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Function.h>
#include <dolfin/nls/NewtonSolver.h>
#include <dolfin/common/Variable.h>

#include <fsm/src/QuadratureFunction.h>
#include <fsm/src/PlasticityProblem.h>

namespace py = pybind11;


//PYBIND11_MAKE_OPAQUE(std::vector<double>);

namespace fsm_wrappers
{
  void return_mapping(py::module& m)
  {
    py::class_<fsm::ReturnMapping, std::shared_ptr<fsm::ReturnMapping>, dolfin::Variable>
      (m, "ReturnMapping", "Class ReturnMapping")
      .def(py::init<const unsigned int>(), "Create a ReturnMapping instance")
      .def("closest_point_projection", &fsm::ReturnMapping::closest_point_projection)
    ;
  }
}
