// Copyright (C) 2006-2010 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2010-01-03

#ifndef __VON_MISESG_H
#define __VON_MISESG_H

#include <Eigen/Dense>
#include "fsm/src/PlasticityModel.h"
#include "fsm/src/UFLQuadratureFunction.h"
namespace fsm
{

  class VonMisesG : public PlasticityModel
  {
  public:

    /// Delete copy constructor and assignement
    VonMisesG& operator=(const VonMisesG&) = delete;  // Disallow copying
    VonMisesG(const VonMisesG&) = delete;
   // in python u define
   // def nu_tempdep(t):
   //      return .. expression  t
   // fsm.UFLquadratureFunction(nu_tempdep, element_temperature,mesh)
   // plasticmodel = fsm.plasticmodel.VonMisesG()..
   // ConstituUpdate(plasticmodel)

   
    /// Constructor
    VonMisesG(std::shared_ptr<const UFLQuadratureFunction> qdef_E, std::shared_ptr<const UFLQuadratureFunction> qdef_nu,
             std::shared_ptr<const UFLQuadratureFunction> qdef_yield_stress,            std::shared_ptr<const UFLQuadratureFunction>  qdef_hardening_parameter);
  /*
: _qdef_E(qdef_E),....
{
  A.setZero();
  A(0, 0) =  2; A(1, 1) =  2; A(2, 2) =  2;
  A(3, 3) =  6; A(4, 4) =  6; A(5, 5) =  6;
  A(0, 1) = -1; A(0, 2) = -1; A(1, 0) = -1;
  A(1, 2) = -1; A(2, 0) = -1; A(2, 1) = -1;
}


  */


 // Projection. 
 // python => temp_q= FunctionSpace(FiniteElement("Quadrature",degre=4..., scheme), mesh)
 // python => stress_q= FunctionSpace(FiniteElement("Quadrature",degre=2..., scheme), mesh)
 // project temp in the stress.. then assign in UFLQuadrature.
 
 // code of Jack ( condesation).-> massmatrix (Stress, Temperature)   element level.
 // 



    // Update constant inside
  //restrict ULF
    void update_parameters(const dolfin::Cell& cell, const double* coordinates, const ufc::cell& ufc_cell, const size_t  ip=0, bool samequadrature=true) const; //call inside constructor of PlasticityModel.	
     // 
     /*
  {
   // Compute number of quadrature points per cell
  _num_ip_dofs = _qdef_nu->element()->value_dimension(0);
  _num_ip_per_cell = _qdef_nu->element()->space_dimension()/_num_ip_dofs;
  _nu_vector.resize(_num_ip_dofs*_num_ip_per_cell);  

  std::vector<double> _E_vector;
  _E_vector.resize(_num_ip_dofs*_num_ip_per_cell);  //scalar
  _qdef_E->restrict(&_E_vector[0], *_qdef_E->element(), cell, vertex_coordinates, ufc_cell);
  std::vector<double> _nu_vector;
  _nu_vector.resize(_num_ip_dofs*_num_ip_per_cell);  //vector of size=1
  _qdef_nu->restrict(&_nu_vector[0], *_qdef_nu->element(), cell, vertex_coordinates, ufc_cell);

  //nu_vector E_vector
  // P1=> 3Dof  same order of Temperature and stress.

  
  _tangent_plasticity = PlasticityModel(_E_vector[ip],_nu_vector[ip])->elastic_tangent;
  // _qdef_hardening and _qdef_yieldstress call restriction and then pass 1st value
  // assign to member of VonMisesG 
  _yield_stress= ...                                           
  _hardening_parameter = ...                                    

  }                                  

    */    


    /// Hardening parameter
    double hardening_parameter(double eps_p_eq) const;

    /// Value of yield function f
    inline double f(const Eigen::Matrix<double, 6, 1>& stress,
                    double eps_p_eq) const;

    /// First derivative of f with respect to sigma
    inline void df(Eigen::Matrix<double, 6, 1>& df_dsigma,
                   const Eigen::Matrix<double, 6, 1>& stress) const;

    /// Second derivative of g with respect to sigma
    inline void ddg(Eigen::Matrix<double, 6, 6>& ddg_ddsigma,
                    const Eigen::Matrix<double, 6, 1>& stress) const;
    
  
	

  private:

    // Computes effective stresses
    inline double
      effective_stress(const Eigen::Matrix<double, 6, 1>& stress) const;

    // Model parameters
    const double _qdef_yield_stress, _qdef_nu, _qdef_E, _qdef_hardening_parameter;
    // Model parameters E, nu
    double _yield_stress, _hardening_parameter;
    

    // Auxiliary variables
    Eigen::Matrix<double, 6, 6> A;
  };
}
#endif
