// Copyright (C) 2006-2010 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2010-01-03

#ifndef __CONSTITUTIVE_UPDATE_H
#define __CONSTITUTIVE_UPDATE_H

#include <vector>
#include <Eigen/Dense>

#include <boost/multi_array.hpp>
#include "fsm/src/HistoryData.h"
#include "fsm/src/ReturnMapping.h"
#include "fsm/src/StateUpdate.h"

namespace dolfin
{
  class Cell;
  class FiniteElement;
  class Function;
  class GenericDofMap;
}

namespace fsm
{

  class PlasticityModel;
  class UFLQuadratureFunction;

  class ConstitutiveUpdate : public StateUpdate
  {
  public:

    /// Delete copy constructor and assignement
    ConstitutiveUpdate& operator=(const ConstitutiveUpdate&) = delete;  // Disallow copying
    ConstitutiveUpdate(const ConstitutiveUpdate&) = delete;

    /// Constructor
    ConstitutiveUpdate(std::shared_ptr<const UFLQuadratureFunction> strain_func,
                       const std::vector<std::shared_ptr<const PlasticityModel>> plastic_model_per_cell);

    /// Destructor
    ~ConstitutiveUpdate();

    /// Update stress for cell
    void update(const dolfin::Cell& cell,
                const double* vertex_coordinates);

    /// Update history variables
    void update_history();

    const std::vector<double>& w_stress() const
    { return _w_stress; }

    const std::vector<double>& w_tangent() const
    { return _w_tangent; }

    std::shared_ptr<const HistoryData> eps_p_eq() const
    { return _eps_p_equiv; }

    std::shared_ptr<const HistoryData> eps_p() const
    { return _eps_p; }
	
    std::size_t num_ip_per_cell() const
    { return _num_ip_per_cell; }

    const bool get_plastic_last(const std::size_t cell_index, std::size_t ip) const
    {   
        //std::cout<<cell_index<<"	"<<ip<<std::endl;
	//std::cout<<"Plastic last 1 : 	"<<_plastic_last[cell_index][ip]<<std::endl;
	/*std::cout<<"####################"<<std::endl;
        std::cout<<"Plastic last 1 : 	"<<_plastic_last[2883][0]<<std::endl;
	std::cout<<"Plastic last 1 : 	"<<_plastic_last[2883][1]<<std::endl;
	std::cout<<"Plastic last 1 : 	"<<_plastic_last[2883][2]<<std::endl;
	std::cout<<"Plastic last 1 : 	"<<_plastic_last[2883][3]<<std::endl;
	std::cout<<"Plastic last 1 : 	"<<_plastic_last[2883][4]<<std::endl;
	std::cout<<"####################"<<std::endl;*/
	//std::cout<<"_plastic_last[cell_index][ip]	"<< typeid ( _plastic_last[cell_index][ip] ).name() <<std::endl;
	return _plastic_last[cell_index][ip];
    }

    void update_plastic_last(std::size_t cell_index, std::size_t ip, const bool act)
   {
	/*if(act==0)
      		_plastic_last[cell_index][ip] = act;
	else if(act==1)
      		_plastic_last[cell_index][ip] = act;
	else
	{
		std::cout << "#################################################################\n";
		std::cout << "Cannot transfer data of plastified state (_plastic_last) at some ip\n";
		std::cout << "#################################################################\n";
	}*/
	_plastic_last[cell_index][ip] = act;
   }

    void update_eps_p_eq(std::size_t cell_index, std::size_t ip, Eigen::Matrix<double, 1, 1> eps_p_old_values);/*const std::shared_ptr<const HistoryData> eps_p_old, std::size_t cell_index_old,  )*/
    void update_eps_p(std::size_t cell_index, std::size_t ip, Eigen::Matrix<double, 6, 1> eps_p_old_values);
    

    /*void set_new_elastic_tangent(std::shared_ptr<const PlasticityModel> plastic_model_current_cell)  // update the elastic tangent at every cell
  {
    // Update values in current Elastic tangent with cell based plasticity model.
    _De (plastic_model_current_cell->elastic_tangent());
  }*/ // not working

  private:
    // Index of previously computed cell
    std::size_t _prev_cell_index;
    // Previously computed strain
    std::vector<double> _prev_w_strain;

    
    // Restriction to UFC cell
    std::vector<double> _w_strain;
    std::vector<double> _w_stress;
    std::vector<double> _w_tangent;
   

    // Strain tensor UFLQuadratureFunction
    std::shared_ptr<const UFLQuadratureFunction> _strain_func;
    std::vector<std::shared_ptr<const PlasticityModel>> _plastic_model_per_cell; // to have the material properties for each cell

    // Plasticity model
    //std::shared_ptr<const PlasticityModel> _plastic_model;//,_plastic_model2;

    // Return mapping object
    ReturnMapping return_mapping;

    // Internal variables
    std::shared_ptr<HistoryData> _eps_p;
    std::shared_ptr<HistoryData> _eps_p_equiv;

    // Track points that deformed plastically at end of last increment
    boost::multi_array<bool, 2> _plastic_last;

    std::size_t _num_ip_dofs;
    std::size_t _num_ip_per_cell;

    // Stress quadrature points in reference coordinates
    boost::multi_array<double, 2> _X;

    // Elastic tangent
    //const Eigen::Matrix<double, 6, 6>& _De;
    Eigen::Matrix<double, 6, 6> _De_of_local_cell; // declaring on the cell level and not reference pointer thing

  };
}

#endif
