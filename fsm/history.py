# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.

import ufl
import dolfin



from fsm.python import cpp
history_data = cpp.history_data

#       .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
#            std::shared_ptr<const dolfin::FiniteElement>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")
#       .def(py::init<std::shared_ptr<const dolfin::Mesh>,
#            std::shared_ptr<const dolfin::FiniteElement>,
#            std::shared_ptr<fsm::StateUpdate>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")

class HistoryData(ufl.Coefficient):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        if not isinstance(args[0], history_data.HistoryData):
            raise RuntimeError("Expecting a 'fsm.history_data.history_data.HistoryData' as argument 1, got ", type(args[0]), ".")
        else:
            self._cpp_object = args[0]
        if not isinstance(args[1], dolfin.function.functionspace.FunctionSpace):
            raise RuntimeError("Expecting a 'dolfin FunctionSpace' as argument 2, got ", type(args[1]), ".")

        ufl.Coefficient.__init__(self, args[1].ufl_function_space(), count=self._cpp_object.id())

        # Set name as given or automatic
        name = kwargs.get("name") or "f_%d" % self.count()
        self._cpp_object.rename(name, "a HistoryData")

    def name(self):
        return self._cpp_object.name()
    
    def cpp_object(self):
        return self._cpp_object

    def value_rank(self):
        return self._cpp_object.value_rank()

    def value_dimension(self, i):
        return self._cpp_object.value_dimension(i)

    def value_shape(self):
        return self._cpp_object.value_shape
