# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.

import ufl
import dolfin
from fsm.python import cpp
status_update = cpp.status_update
quadrature_function = cpp.quadrature_function

from fsm import update

#       .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
#            std::shared_ptr<const dolfin::FiniteElement>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")
#       .def(py::init<std::shared_ptr<const dolfin::Mesh>,
#            std::shared_ptr<const dolfin::FiniteElement>,
#            std::shared_ptr<fsm::StateUpdate>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")

class QuadratureFunction(ufl.Coefficient):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        
        if not isinstance(args[0], dolfin.function.functionspace.FunctionSpace):
            raise RuntimeError("Expecting a 'dolfin FunctionSpace' as argument 1, got ", type(args[0]), ".")
        self.Mesh = args[0].mesh()
        self.Element = args[0].element()
        if not isinstance(args[1], status_update.DoubleVector):
            raise RuntimeError("Expecting a 'fsm.fsm_cpp.status_update.DoubleVector' as argument 1, got ", type(args[1]), ".")
        if len(args) == 3:
            if not isinstance(args[2], update.ConstitutiveUpdateBaseClass):
                raise RuntimeError("Expecting a 'fsm class derived from ConstitutiveUpdateBaseClass' as argument 2, got ", type(args[2]), ".")
            self.ConstitutiveUpdate = args[2]
            if self.ConstitutiveUpdate.AlreadyLinked:
                dolfin.cpp.warning("")
                dolfin.cpp.warning("The 'ConstitutiveUpdate' " + str(self.ConstitutiveUpdate))
                dolfin.cpp.warning("has already been linked to a 'QuadratureFunction'")
                dolfin.cpp.warning("This is not necessarily an error, but you must know what you are doing")
                dolfin.cpp.warning("")
#                 raise RuntimeError("The 'ConstitutiveUpdate' ", self.ConstitutiveUpdate, " has already been linked to a 'QuadratureFunction'")
            else:
                self.ConstitutiveUpdate.AlreadyLinked = True
            self._cpp_object = quadrature_function.QuadratureFunction(self.Mesh, self.Element,
                                            self.ConstitutiveUpdate.cpp_object(), args[1])
        elif len(args) == 2:
            self._cpp_object = quadrature_function.QuadratureFunction(self.Mesh, self.Element, args[1])
        else:
            raise RuntimeError("Expecting 2 or 3 arguments, got ", len(args), ".")

        ufl.Coefficient.__init__(self, args[0].ufl_function_space(), count=self._cpp_object.id())

        # Set name as given or automatic
        name = kwargs.get("name") or "f_%d" % self.count()
        self._cpp_object.rename(name, "a QuadratureFunction")

    def name(self):
        return self._cpp_object.name()
    
    def cpp_object(self):
        return self._cpp_object

    def value_rank(self):
        return self._cpp_object.value_rank()

    def value_dimension(self, i):
        return self._cpp_object.value_dimension(i)

    def value_shape(self):
        return self._cpp_object.value_shape
