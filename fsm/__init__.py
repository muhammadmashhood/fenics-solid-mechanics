# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.



# Reset dl open flags
# sys.setdlopenflags(stored_dlopen_flags)
# del sys

# Import cpp modules
#from .fsm_cpp.quadrature_function import (QuadratureFunction)
#from .fsm_cpp.plasticity_problem import (PlasticityProblem)


#from  fsm_cpp.plasticity_model import (PlasticityModel, DruckerPrager, VonMises)
#from .fsm_cpp.history_data import (HistoryData)
#from .fsm_cpp.status_update import (StateUpdate, ConstitutiveUpdate)
#from .fsm_cpp.status_update import (StateUpdate)
#from .fsm_cpp.return_mapping import (ReturnMapping)

from fsm.ufl_quadrature import UFLQuadratureFunction
from fsm.quadrature import QuadratureFunction
from fsm.update import (ConstitutiveUpdate, AnbaConstitutiveUpdate, AnbaConstitutiveUpdateCubic)
from fsm.problem import PlasticityProblem
from fsm.history import HistoryData

__all__ = [
     'UFLQuadratureFunction',
     'QuadratureFunction',
     'ConstitutiveUpdate',
     'AnbaConstitutiveUpdate',
     'AnbaConstitutiveUpdateCubic',
     'PlasticityProblem',
     'HistoryData'
     ]

