# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.

import ufl
import dolfin
from fsm.python import cpp

ufl_quadrature_function = cpp.ufl_quadrature_function


class UFLQuadratureFunction(ufl.Coefficient):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        
        mesh_el_mapper = {dolfin.CellType.Type.hexahedron   : dolfin.UnitCubeMesh.create(dolfin.MPI.comm_self,1,1,1,dolfin.CellType.Type.hexahedron),
                          dolfin.CellType.Type.interval     : dolfin.UnitIntervalMesh.create(dolfin.MPI.comm_self,1),
                          dolfin.CellType.Type.quadrilateral: dolfin.UnitSquareMesh.create(dolfin.MPI.comm_self,1,1,dolfin.CellType.Type.quadrilateral),
                          dolfin.CellType.Type.tetrahedron  : dolfin.UnitTetrahedronMesh.create(),
                          dolfin.CellType.Type.triangle     : dolfin.UnitTriangleMesh.create()}

        if len(args) != 3:
            raise RuntimeError("Expecting 3 arguments, got ", len(args), ".")
#        if not isinstance(args[0], dolfin.function.functionspace.FunctionSpace):
#            raise RuntimeError("Expecting a 'dolfin FunctionSpace' as argument 1, got ", type(args[0]), ".")
        self.UFLExpression = args[0]
        if not isinstance(args[1], ufl.finiteelement.finiteelement.FiniteElementBase):
            raise RuntimeError("Expecting a 'dolfin FiniteElement' as argument 2, got ", type(args[1]), ".")
        self.Element = args[1]
        if not isinstance(args[2], dolfin.Mesh):
            raise RuntimeError("Expecting a 'dolfin Mesh' as argument 3, got ", type(args[2]), ".")
        self.Mesh = args[2]

        representation = 'quadrature'
        quadrature_degree = self.Element.degree()

        # Reference triangle mesh, TestFunction and CellVolume
        mesh_reference = mesh_el_mapper[self.Mesh.type().cell_type()]
        #import pdb
        #pdb.set_trace()# disabling to stop bothering each time solver is re-initialized
        Vq_reference = dolfin.FunctionSpace(mesh_reference, self.Element)
        tq_reference = dolfin.TestFunction(Vq_reference)
        cell_volume_reference = dolfin.CellVolume(mesh_reference)

        # Constant with shape == self.quadrature_element.value_shape() filled with ones
        shape = self.Element.value_shape()
        ones = 1.
        for i in range(len(shape)-1,-1,-1):
            ones = tuple([ones] * shape[i])
        ones = dolfin.Constant(ones)

        # Form define n te reference triangle
        res_reference = dolfin.Form(ufl.inner(tq_reference, ones) / cell_volume_reference * ufl.dx(mesh_reference),
                                    form_compiler_parameters={
                                            "representation": representation,
                                            "quadrature_degree": quadrature_degree})

        # Define the form on the real mesh
        self.Vq = dolfin.FunctionSpace(self.Mesh, self.Element)
        self.v = dolfin.TestFunction(self.Vq)
        self.cell_volume = dolfin.CellVolume(self.Mesh)
        self.function_space = dolfin.FunctionSpace(self.Mesh, self.Element)
        self.form = dolfin.Form(ufl.inner(self.v, self.UFLExpression) / self.cell_volume * ufl.dx(self.Mesh),
                                form_compiler_parameters={
                                        "representation": representation,
                                        "quadrature_degree": quadrature_degree})

        self._cpp_object = ufl_quadrature_function.UFLQuadratureFunction(self.form, res_reference)

        ufl.Coefficient.__init__(self, self.function_space, count=self._cpp_object.id())

        # Set name as given or automatic
        name = kwargs.get("name") or "f_%d" % self.count()
        self._cpp_object.rename(name, "an UFLQuadratureFunction")

    def name(self):
        return self._cpp_object.name()
    
    def cpp_object(self):
        return self._cpp_object

    def value_rank(self):
        return self._cpp_object.value_rank()

    def value_dimension(self, i):
        return self._cpp_object.value_dimension(i)

    def value_shape(self):
        return self._cpp_object.value_shape


